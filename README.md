# Deprecated

QCoreApplicationForNonQtUsage is now part of [MdtApplication](https://gitlab.com/scandyna/mdtapplication)
and will no longer be maintained.

# QCoreApplicationForNonQtUsage

Provide a QCoreApplication for a non Qt application.

For the available classes, functions, and their usage,
see [the API documentation](https://scandyna.gitlab.io/qcoreapplicationfornonqtusage)

# Required tools and libraries

Some tools and libraries are required to build QCoreApplicationForNonQtUsage:
 - Git
 - CMake
 - Conan (optional)
 - A compiler (Gcc or Clang or MSVC)
 - Qt5 (optional, can be managed by Conan)
 - Make (optional)

For a overview how to install them, see https://gitlab.com/scandyna/build-and-install-cpp

# Usage

In your source directory, create a CMakeLists.txt:
```cmake
cmake_minimum_required(VERSION 3.10)
project(MyApp)

find_package(Threads REQUIRED)
find_package(Qt5 COMPONENTS Core REQUIRED)
find_package(MdtQt0 COMPONENTS QCoreApplicationForNonQtUsage REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

add_executable(myApp myApp.cpp)
target_link_libraries(myApp MdtQt0::QCoreApplicationForNonQtUsage)
```

The easiest way is to use Conan to manage dependencies.

## Use a allready installed Qt5

In your source directory, create a conanfile.txt:
```conan
[requires]
QCoreApplicationForNonQtUsage/x.y.z@scandyna/testing

[options]

[generators]
cmake_paths
```

Create a build directory and cd to it:
```bash
mkdir build && cd build
```

Install the dependencies:
```bash
conan install -s build_type=Release --build=missing ..
```

Notice that we do not depend on Qt5 in the `conanfile.txt`.
Here it is assumed that Qt5 is allready installed.

Configure your project:
```bash
cmake -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DCMAKE_PREFIX_PATH=~/opt/qt/Qt5/5.13.1/gcc_64 -DCMAKE_BUILD_TYPE=Release ..
```

## Manage Qt5 as a Conan dependency

Update the `conanfile.txt`:
```conan
[requires]
QCoreApplicationForNonQtUsage/x.y.z@scandyna/testing
qt/[>=5.12]@bincrafters/stable

[options]
QCoreApplicationForNonQtUsage:use_conan_qt=True

[generators]
cmake_paths
```

Install the dependencies:
```bash
conan install -s build_type=Release --build=missing ..
```

Configure your project:
```bash
cmake -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DCMAKE_BUILD_TYPE=Release ..
```

Here, because we use Conan to manage Qt dependency,
cmake will receive the Qt path in its `CMAKE_PREFIX_PATH`.

Note: to avoid rebuilding Conan Qt every time,
using profiles is a good solution.
See:
 - [Conan profiles](https://docs.conan.io/en/latest/using_packages/using_profiles.html) documentation
 - [scandyna conan-config](https://gitlab.com/scandyna/conan-config)

# Available conan options

| Option           | Default | Possible Values  | Explanations |
| -----------------|:------- |:----------------:|--------------|
| shared           | True    |  [True, False]   | Build as shared library |
| build_tests      | False   |  [True, False]   | If True, will also require Catch2 |
| use_conan_qt     | False   |  [True, False]   | Use [conan Qt](https://github.com/bincrafters/conan-qt) as conan dependency. |


# Build QCoreApplicationForNonQtUsage

## Get the source code

Go to a folder and clone the source code:
```bash
git clone https://gitlab.com/scandyna/qcoreapplicationfornonqtusage.git
```

## Note about install prefix

Some note on the `CMAKE_INSTALL_PREFIX`:
 - To target a system wide installation on Linux, set it to `/usr` (`-DCMAKE_INSTALL_PREFIX=/usr`) .
 - For other locations, spcecify also the <package-name>, (for example `-DCMAKE_INSTALL_PREFIX=~/opt/Mdt0Qt5`).

For details about that, see:
 - https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtInstallDirs.html
 - https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_PREFIX.html
 - https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtInstallLibrary.html

## Install using CMake on Linux (Makefiles)

Create a build directory and go to it:
```bash
mkdir build
cd build
```

### Setup for the default compiler (probably Gcc)

Install the required dependencies:
```bash
conan install -s build_type=Release -o build_tests=True --build=missing ..
```

Configure the project:
```bash
cmake -DCMAKE_INSTALL_PREFIX=/some/path -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DQT_PREFIX_PATH=~/opt/qt/Qt5/5.13.1/gcc_64 -DCMAKE_BUILD_TYPE=Release ..
cmake-gui .
```

### Setup to build with ThreaSanitizer

This requires modifications in the `settings.yml` Conan configuration,
and also some profile files.
See my [conan-config repository](https://gitlab.com/scandyna/conan-config) for more informations.

#### For Gcc

Install the required dependencies:
```bash
conan install --profile linux_gcc8_x86_64_tsan -o use_conan_qt=True -o build_tests=True  ..
```

Configure the project:
```bash
cmake -DCMAKE_INSTALL_PREFIX=/some/path -DCMAKE_C_COMPILER=gcc-8 -DCMAKE_CXX_COMPILER=g++-8 -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DSANITIZER_ENABLE_THREAD=ON ..
cmake-gui .
```

#### For Clang

Install the required dependencies:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_tsan -o use_conan_qt=True -o build_tests=True  ..
```

Configure the project:
```bash
cmake -DCMAKE_INSTALL_PREFIX=/some/path -DCMAKE_C_COMPILER=clang-6.0 -DCMAKE_CXX_COMPILER=clang++-6.0 -DCMAKE_C_FLAGS_INIT=-stdlib=libc++ -DCMAKE_CXX_FLAGS_INIT=-stdlib=libc++ -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DSANITIZER_ENABLE_THREAD=ON ..
cmake-gui .
```

### Build and install

Compile:
```bash
make -j4
```

To run the tests:
```bash
ctest --output-on-failure -j4 .
```

Install:
```bash
make install
```

## Install using CMake on Windows MinGW

Open a terminal that has gcc and mingw32-make in the PATH.

Create a build directory and go to it:
```bash
mkdir build
cd build
```

Install the required dependencies:
```bash
conan install --profile windows_gcc7_x86_64 -s build_type=Release --build=missing ..
```

Configure the project:
```bash
cmake -G"MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=C:\some\path -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DQT_PREFIX_PATH="C:\Qt\5.13\mingw73_64" -DCMAKE_BUILD_TYPE=Release ..
cmake-gui .
```

If `QT_PREFIX_PATH` was not specified, and no Qt5 installation is in the `PATH`,
a error will probably occur, telling that Qt was not found.
Set the `QT_PREFIX_PATH` by choosing the path to the Qt5 installation,
then run "Configure".

Also choose different options, like the components to build.
Once done, run "Generate", then quit cmake-gui.

Compile:
```bash
mingw32-make -j4
```

To run the tests:
```bash
ctest --output-on-failure -j4 .
```

Install:
```bash
mingw32-make install
```

## Install using CMake on Windows MSVC

Create a build directory and go to it:
```bash
mkdir build
cd build
```

Install the required dependencies:
```bash
conan install --profile windows_msvc15_x86_64 -s build_type=Release --build=missing ..
```

Configure:
```cmd
cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_INSTALL_PREFIX=C:\some\path -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -DQT_PREFIX_PATH="C:\Qt\5.13\msvc2017_64"  ..
cmake-gui .
```

TODO: check "Visual Studio 15 2017 Win32"

To generate the tests, run the build:
```bash
cmake --build . --config Release
```

To run the tests:
```bash
ctest --output-on-failure -C Release -j4 .
```

Install:
```cmd
cmake --build . --target INSTALL --config Release
```

# Create a Conan package

The package version is picked up from git tag.
If working on QCoreApplication, go to the root of the source tree:
```bash
git tag x.y.z
conan create . scandyna/testing
```

To create other packages:
```bash
conan create . x.y.z@user/channel
```

Above examples will generate a package that uses the Qt version that is installed on the system.

To create packages that depend on Conan Qt:
```bash
conan create . scandyna/testing -o QCoreApplicationForNonQtUsage:use_conan_qt=True
```

Because Qt offers binary compatibility,
it should not be required to create package for each minor Qt version,
but more a package per compiler and other things that breacks binary compatibility.
