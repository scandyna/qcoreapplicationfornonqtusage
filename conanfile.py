from conans import ConanFile, CMake, tools
import os


class QCoreApplicationForNonQtUsageConan(ConanFile):
  name = "QCoreApplicationForNonQtUsage"
  #version = "0.1"
  license = "BSD 3-Clause"
  url = "https://gitlab.com/scandyna/qcoreapplicationfornonqtusage"
  description = "Provide a QCoreApplication for a non Qt application"
  settings = "os", "compiler", "build_type", "arch"
  options = {"shared": [True, False],
             "build_tests": [True, False],
             "use_conan_qt": [True, False]}
             #"conan_qt_version": "ANY"}
  default_options = {"shared": True,
                     "build_tests": False,
                     "use_conan_qt": False}
                     #"conan_qt_version": "None"}
  requires = "MdtCMakeModules/[>=0.11.0]@scandyna/testing"
  generators = "cmake_paths", "virtualenv"
  exports_sources = "cmake/Modules/*", "src/*", "CMakeLists.txt", "conanfile.py", "LICENSE"
  # If no_copy_source is False, conan copies sources to build directory and does in-source build,
  # resulting having build files installed in the package
  # See also: https://github.com/conan-io/conan/issues/350
  no_copy_source = True

  # TODO should fail if no tag found ?
  # Does conan provide a semver tool ??
  def set_version(self):
    if os.path.exists(".git"):
      git = tools.Git()
      self.version = "%s" % (git.get_tag())
    #else:
      #self.version = "None"

  def requirements(self):

    if self.options.build_tests:
      self.requires("Catch2/[>=2.11.1]@catchorg/stable")

    if self.options.use_conan_qt:
      self.requires("qt/[>=5.12.5]@bincrafters/stable")


  def package_id(self):
    del self.info.options.build_tests
    #self.info.requires.remove("Catch2")
    #del self.info.options["qt"].commercial
    del self.info.options["qt"].with_sqlite3
    del self.info.options["qt"].with_mysql
    del self.info.options["qt"].with_pq
    del self.info.options["qt"].with_odbc
    del self.info.options["qt"].with_sdl2
    del self.info.options["qt"].with_libalsa
    del self.info.options["qt"].with_openal
    del self.info.options["qt"].GUI
    del self.info.options["qt"].widgets
    del self.info.options["qt"].qtserialport


  def configure_cmake(self):
    cmake = CMake(self)
    cmake.definitions["CMAKE_TOOLCHAIN_FILE"] = "%s/conan_paths.cmake" % (self.build_folder)
    if self.settings.compiler == "gcc" or self.settings.compiler == "clang":
      if self.settings.compiler.sanitizer == "Thread":
        cmake.definitions["SANITIZER_ENABLE_THREAD"] = "ON"
    return cmake


  def build(self):
    cmake = self.configure_cmake()
    cmake.configure()
    cmake.build()


  def package(self):
    cmake = self.configure_cmake()
    cmake.install()

# See: https://bincrafters.readthedocs.io/en/latest/contributing_to_packages/package_guidelines_required.html
