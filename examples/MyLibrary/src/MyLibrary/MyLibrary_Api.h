/****************************************************************************
 **
 ** Copyright (C) 2019-2020 Philippe Steinmann.
 **
 ** This file is part of QCoreApplicationForNonQtUsage library.
 **
 ** QCoreApplicationForNonQtUsage is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** QCoreApplicationForNonQtUsage is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with QCoreApplicationForNonQtUsage. If not, see <http://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef EXAMPLES_MYLIBRARY_MYLIBRARY_API_H
#define EXAMPLES_MYLIBRARY_MYLIBRARY_API_H

#include "MyLibraryError.h"
#include "examples_mylibrary_export.h"
#include <QObject>
#include <string>

namespace MyLibrary{

  /*! \brief API to the business logic
   */
  class EXAMPLES_MYLIBRARY_EXPORT MyLibrary_Api : public QObject
  {
    Q_OBJECT

   public:

    /*! \brief Send a command
     */
    MyLibraryError sendCommand(const std::string & command);

    /*! \brief Set value
     */
    void setValue(const std::string & val);

    /*! \brief Get value
     */
    const std::string & value() const
    {
      return mValue;
    }

   signals:

    void responseReceived(const std::string & response);

   private slots:

    void onResponseReceived();

   private:

    std::string mValue;
  };

} // namespace MyLibrary{

#endif // #ifndef EXAMPLES_MYLIBRARY_MYLIBRARY_API_H
