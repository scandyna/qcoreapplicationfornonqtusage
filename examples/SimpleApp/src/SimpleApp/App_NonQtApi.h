/****************************************************************************
 **
 ** Copyright (C) 2019-2020 Philippe Steinmann.
 **
 ** This file is part of QCoreApplicationForNonQtUsage library.
 **
 ** QCoreApplicationForNonQtUsage is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** QCoreApplicationForNonQtUsage is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with QCoreApplicationForNonQtUsage. If not, see <http://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_H
#define EXAMPLES_SIMPLE_APP_APP_NON_QT_API_H

#include "AppError.h"
#include "examples_simpleapp_export.h"
#include <string>

namespace SimpleApp{

  /*! \brief API of the SimpleApp example
   *
   * Exposes the functionalities that can be used
   * from a application that has its own event loop
   * (or maybe no event loop at all).
   *
   * This class reflects AppBusinessLogic
   */
  class EXAMPLES_SIMPLEAPP_EXPORT App_NonQtApi
  {
   public:

    /*! \brief Send a command
     */
    AppError sendCommand(const std::string & command);
  };

} // namespace SimpleApp{

#endif // #ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_H
