/****************************************************************************
 **
 ** Copyright (C) 2019-2020 Philippe Steinmann.
 **
 ** This file is part of QCoreApplicationForNonQtUsage library.
 **
 ** QCoreApplicationForNonQtUsage is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** QCoreApplicationForNonQtUsage is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with QCoreApplicationForNonQtUsage. If not, see <http://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_IMPL_H
#define EXAMPLES_SIMPLE_APP_APP_NON_QT_API_IMPL_H

#include "AppError.h"
#include <QObject>
#include <string>

namespace SimpleApp{

  /*! \brief Implementation of App_NonQtApi
   *
   * One advange to seperate the implementation
   * is to have less confusing code.
   * For each command in the public API,
   * that must be called in the thread context of the application using the Qt event loop,
   * a invoke signal will be created.
   * Because signals and slots does not support return values,
   * they are mapped as first argument of the invoke method.
   *
   * \sa App_NonQtApi_Worker
   */
  class App_NonQtApi_Impl : public QObject
  {
   Q_OBJECT

   public:

   signals:

    /*! \brief Invoke sendCommand() in the thread context of AppBusinessLogic
     */
    void invokeSendCommand(SimpleApp::AppError & retVal, const std::string & command);

   private:

    
  };

} // namespace SimpleApp{

#endif // #ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_IMPL_H
