/****************************************************************************
 **
 ** Copyright (C) 2019-2020 Philippe Steinmann.
 **
 ** This file is part of QCoreApplicationForNonQtUsage library.
 **
 ** QCoreApplicationForNonQtUsage is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** QCoreApplicationForNonQtUsage is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with QCoreApplicationForNonQtUsage. If not, see <http://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_WORKER_H
#define EXAMPLES_SIMPLE_APP_APP_NON_QT_API_WORKER_H

#include "AppError.h"
#include <QObject>
#include <string>

namespace SimpleApp{

  /*! \brief Worker for App_NonQtApi
   *
   * The main goal of this worker is to call
   * the methods in the thread context of the application business logic.
   * This is done thanks to the signals and slot mechanism.
   *
   * Because signals and slots does not support return values,
   * they are mapped as first argument of the slots.
   *
   * \sa App_NonQtApi_Impl
   */
  class App_NonQtApi_Worker : public QObject
  {
   Q_OBJECT

   public:

    

   public slots:

    /*! \brief Call AppBusinessLogic::sendCommand()
     */
    void sendCommand(SimpleApp::AppError & retVal, const std::string & command)
    {
    }
  };

} // namespace SimpleApp{

#endif // #ifndef EXAMPLES_SIMPLE_APP_APP_NON_QT_API_WORKER_H
