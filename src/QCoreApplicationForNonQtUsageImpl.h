/****************************************************************************
 **
 ** Copyright (C) 2019-2020 Philippe Steinmann.
 **
 ** This file is part of QCoreApplicationForNonQtUsage library.
 **
 ** QCoreApplicationForNonQtUsage is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** QCoreApplicationForNonQtUsage is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with QCoreApplicationForNonQtUsage. If not, see <http://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef QCORE_APPLICATION_FOR_NON_QT_USAGE_IMPL_H
#define QCORE_APPLICATION_FOR_NON_QT_USAGE_IMPL_H

#include "qcoreapplicationfornonqtusage_export.h"
#include <QObject>
#include <QCoreApplication>

class QCOREAPPLICATIONFORNONQTUSAGE_EXPORT QCoreApplicationForNonQtUsageImpl : public QObject
{
 Q_OBJECT

 public:

  void registerApplication(QCoreApplication *app)
  {
    Q_ASSERT(app != nullptr);

    connect(this, &QCoreApplicationForNonQtUsageImpl::invokeQuit, app, &QCoreApplication::quit, Qt::QueuedConnection);
  }

 Q_SIGNALS:

  void invokeQuit();
};

#endif // #ifndef QCORE_APPLICATION_FOR_NON_QT_USAGE_IMPL_H
