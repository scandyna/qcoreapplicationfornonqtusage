#include <QCoreApplicationForNonQtUsage>
#include <QDebug>

class BasicToyWorker
{
 public:

  BasicToyWorker()
  {
    qDebug() << "Worker start ...";
  }

  ~BasicToyWorker()
  {
    qDebug() << "Worker end";
  }

};

int main(int argc, char *argv[])
{
  QCoreApplicationForNonQtUsage<BasicToyWorker> app;

  return 0;
}
